import * as request from '../constant/fetchRequest'
import HTTP_CODE from '../constant/httpCode'
import {message} from 'antd'
import { convertContent } from '../constant/lang-util'

export const checkValidateCode = (validateCode, captchaId, callback) => {
  return () => {
    (async () => {
      const res = await request.post('/learn/api/auth/verifyCaptcha', {key: captchaId, value: validateCode})
      if (res.status === HTTP_CODE.OK) {
        if (res.body === true) {
          callback()
        } else if (res.body === false) {
          message.warning(convertContent('验证码填写不正确'))
        } else {
          message.warning(convertContent('验证码失效，点击图片刷新'))
        }
      }
    })()
  }
}
