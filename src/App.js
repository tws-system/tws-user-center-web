import React, { Component } from 'react'
import SignInBodyPage from './component/sigin-in/sign-in-body-page'
import { connect } from 'react-redux'
import Redirect from './component/redirect'
import RegistrationBodyPage from './component/register/registration-body-page'
import TwsLayout from './component/tws-layout'
import PasswordRetrieve from './component/password-reset/password-retrieve'
import PasswordReset from './component/password-reset/password-reset'
import LogoutBodyPage from './component/logout-body-page'

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'
import { getLangFromLocalStorage } from './constant/lang-util'
import locales from './locales'
import { LocaleProvider } from 'antd'
import { IntlProvider } from 'react-intl'

class App extends Component {
  render () {
    const lang = this.props.lang || getLangFromLocalStorage()
    const langData = locales[lang]

    return (
      <IntlProvider locale={langData.intlLocale} messages={langData.intlMessage}>
        <LocaleProvider locale={langData.locale}>
          <Router>
            <div>
              <TwsLayout lang={lang}>
                <Route exact path='' component={SignInBodyPage} />
                <Route exact path='/learn/auth/login' component={SignInBodyPage} />
                <Route exact path='/learn/auth/logout' component={LogoutBodyPage} />
                <Route exact path='/learn/auth/redirect' component={Redirect} />
                <Route exact path='/learn/auth/register' component={RegistrationBodyPage} />
                <Route path='/learn/auth/passwordRetrieve' component={PasswordRetrieve} />
                <Route path='/learn/auth/passwordReset' component={PasswordReset} />
              </TwsLayout>
            </div>
          </Router>
        </LocaleProvider>
      </IntlProvider>
    )
  }
}

const mapStateToProps = ({lang}) => ({lang})

export default connect(mapStateToProps)(App)
