import { addLocaleData } from 'react-intl'
import zhLocaleData from 'react-intl/locale-data/zh'

import zh from './zh-Hans-CN.messages'
import zh_TW from './zh-TW.messages'
import en from './en.messages'

addLocaleData(zhLocaleData)

const locales = {
  zh: {
    intlLocale: 'zh-Hans',
    intlMessage: zh
  },
  en: {
    intlLocale: 'en',
    intlMessage: en
  },
  zh_TW: {
    intlLocale: 'zh-TW',
    intlMessage: zh_TW
  }
}

export default locales
