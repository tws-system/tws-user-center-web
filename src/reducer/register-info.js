export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_REGISTER_INFO':
      return Object.assign({}, state, {...action.data})
    default:
      return state
  }
}
