const genSetting = (data) => {
  const { appContextPath } = data
  return Object.assign({}, {
    appContextPath
  }, data)
}

export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_SETTINGS':
      return genSetting(action.data)
    default:
      return state
  }
}
