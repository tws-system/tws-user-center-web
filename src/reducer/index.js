import {combineReducers} from 'redux'

import user from './user'
import settings from './settings'
import lang from './language'
import registerInfo from './register-info'
import socialInfo from './social-info'

export default combineReducers({
  user,
  settings,
  lang,
  registerInfo,
  socialInfo
})
