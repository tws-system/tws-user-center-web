export default (state = {}, action) => {
  switch (action.type) {
    case 'GET_SOCIAL_INFO':
      return action.data
    default:
      return state
  }
}
