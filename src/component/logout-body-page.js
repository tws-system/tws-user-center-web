import React, { Component } from 'react'
import { Spin } from 'antd'
import { withRouter } from 'react-router-dom'

class LogoutBodyPage extends Component {
  clearJWT () {
    window.localStorage.setItem('jwt', '')
    this.props.history.push('/learn/auth/login')
  }

  render () {
    this.clearJWT()
    return <div style={{textAlign: 'center'}}><Spin /></div>
  }
}
export default withRouter(LogoutBodyPage)
