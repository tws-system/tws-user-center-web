import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import '../../style/sign-in-body-page.css'
import SocialSignInBox from './social-sign-in-box'
import SignInBox from './sign-in-box'
import {login} from '../../action/user'
import { Col } from 'antd'
import {checkValidateCode} from '../../action/verification'

class SignInBodyPage extends Component {
  render () {
    const {settings} = this.props
    const { appContextPath } = settings

    return <Col span={6} offset={9} className='sign-in-box'>
      <SignInBox
        checkValidateCode={this.props.checkValidateCode}
        login={this.props.login} />
      <SocialSignInBox appContextPath={appContextPath} />
    </Col>
  }
}
const mapStateToProps = ({settings}) => ({settings})
const mapDispatchToProps = dispatch => ({
  checkValidateCode: (validateCode, captchaId, callback) => dispatch(checkValidateCode(validateCode, captchaId, callback)),
  login: (user, programCodeSearch) => dispatch(login(user, programCodeSearch))
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignInBodyPage))
