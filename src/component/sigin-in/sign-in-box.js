import React from 'react'
import {Button, Col, Form, Icon, Input, Row} from 'antd'
import { FormattedMessage } from 'react-intl'
import { withRouter } from 'react-router-dom'
import {CAPTCHA_URL} from '../../constant/captcha-url'
import {guid} from '../../utils/guid'

const FormItem = Form.Item
class SignInBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      captchaId: guid(),
      captchaUrl: CAPTCHA_URL
    }
  }
  getProgramCode (props) {
    const param = props.location.search
    return param.includes('code') ? param : ''
  }

  handleSubmit (e) {
    const programCodeSearch = this.getProgramCode(this.props)
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const {captcha} = values
        const {captchaId} = this.state
        this.props.checkValidateCode(captcha, captchaId, () => {
          this.props.login({...values}, programCodeSearch)
        })
        this.setState({captchaId: guid()})
      }
    })
  }
  handleImg () {
    this.setState({captchaId: guid()})
  }

  render () {
    const {captchaUrl, captchaId} = this.state
    const url = captchaUrl + '/' + captchaId
    const {getFieldDecorator} = this.props.form
    return <Form className='login-form'>
      <FormItem>
        {getFieldDecorator('username', {
          rules: [{required: true, message: <FormattedMessage id='请输入用户名或邮箱' />}]
        })(
          <Input
            size='large'
            prefix={<Icon type='user' className='login-form__input__icon' />}
            placeholder='Username or email' />
        )}
      </FormItem>
      <FormItem>
        {getFieldDecorator('password', {
          rules: [{required: true, message: <FormattedMessage id='请输入密码' />}]
        })(
          <Input
            size='large'
            prefix={<Icon type='lock' className='login-form__input__icon' />}
            type='password' placeholder='Password' />
        )}
      </FormItem>
      <FormItem>
        <Row gutter={8}>
          <Col span={12}>
            {getFieldDecorator('captcha', {
              rules: [{ required: true, message: <FormattedMessage id='请输入验证码' /> }]
            })(<Input
              size='large'
              prefix={<Icon type='safety' className='login-form__input__icon' />}
              type='text' placeholder='Captcha' />)}
          </Col>
          <Col span={12}>
            <img height='40' width='150' style={{cursor: 'pointer'}}
              src={url}
              onClick={this.handleImg.bind(this)}
            />
          </Col>
        </Row>
      </FormItem>
      <FormItem>
        <Button
          type='primary'
          htmlType='submit'
          size='large'
          onClick={this.handleSubmit.bind(this)}
          className='login-form__submit-button'>
          <FormattedMessage id='登录' />
        </Button>
        <FormattedMessage id='或' />
        <a href='/learn/auth/register?org=1568.1747954.1'><FormattedMessage id='注册' /></a>
        <a className='login-form__forgot-link' href='/learn/auth/passwordRetrieve'>
          <FormattedMessage id='忘记密码' />
        </a>
      </FormItem>
    </Form>
  }
}
const LoginBoxForm = Form.create()(SignInBox)

export default withRouter(LoginBoxForm)
