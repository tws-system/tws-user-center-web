import React from 'react'
import { Divider, Icon } from 'antd'
import { FormattedMessage } from 'react-intl'
import { withRouter } from 'react-router-dom'

class SocialSignInBox extends React.Component {
  getEnv () {
    const {appContextPath} = this.props
    return appContextPath === '/learn' ? 'learn' : 'staging'
  }
  render () {
    const env = this.getEnv()
    const requestQRCodePage = `/learn/api/auth/QRCode/${env}`

    return <div>
      <Divider className='social-sign-in-box__divider'>
        <FormattedMessage id='社交账号登录' />
      </Divider>
      <a href={requestQRCodePage}>
        <Icon className='social-sign-in-box__wechat-icon'
          type='wechat'
          theme='outlined' /></a>
    </div>
  }
}

export default withRouter(SocialSignInBox)
