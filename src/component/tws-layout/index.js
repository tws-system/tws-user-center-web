import React, {Component} from 'react'
import {Col, Layout, Row} from 'antd'
import logo from '../../images/tws-logo-black.png'
import '../../style/tws-layout.less'
import {connect} from 'react-redux'
import LanguageButtons from './language-buttons'
import {withRouter} from 'react-router-dom'

const { Header, Content, Footer } = Layout

class TwsLayout extends Component {
  render () {
    const currentYear = new Date().getFullYear()

    return (
      <Layout>
        <Header className='app-header'>
          <Row>
            <Col span={12} offset={6}>
              <img src={logo} className='app-logo' alt='logo' />
            </Col>

            <Col span={5}>
              <LanguageButtons />
            </Col>
          </Row>
        </Header>
        <Content className='app-content'>
          {this.props.children}
        </Content>
        <Footer className='app-footer'>
          <span>ThoughtWorks Learning Platform ©{currentYear}</span>|
          <a href='http://www.beian.miit.gov.cn'>陕ICP备13005347号-3</a>
        </Footer>
      </Layout>
    )
  }
}
const mapStateToProps = ({user, notifications, settings}) => ({user, notifications, settings})
const mapDispatchToProps = dispatch => ({
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TwsLayout))
