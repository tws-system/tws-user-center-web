import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {wechatLogin} from '../action/user'
import { Spin } from 'antd'

class Redirect extends Component {
  componentDidMount () {
    const code = this.getQueryString('code')
    this.props.wechatLogin(code, () => {
      this.props.history.push(`/learn/auth/register?org=1568.1747954.1`)
    })
  }
  getQueryString (name) {
    const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
    const params = this.props.location.search.substr(1).match(reg)
    if (params != null) return unescape(params[2]); return null
  }

  render () {
    return <div style={{textAlign: 'center'}}><Spin /></div>
  }
}
const mapDispatchToProps = dispatch => ({
  wechatLogin: (code, callback) => dispatch(wechatLogin(code, callback))
})

export default withRouter(connect(null, mapDispatchToProps)(Redirect))
