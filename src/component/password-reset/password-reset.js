import React, { Component } from 'react'
import { Form, Input, Button, Row, Col } from 'antd'
import { connect } from 'react-redux'
import queryString from "query-string";
import '../../style/password-reset.css'
import {FormattedMessage} from "react-intl";
import { submitPassword } from '../../action/user'

const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 8},
    lg: {span: 9},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 14},
    lg: {span: 6},
  },
}

class PasswordReset extends Component {
  constructor (props) {
    super(props)
    this.state = {
      uuId: '',
      confirmDirty: false
    }
  }

  componentDidMount () {
    const {location} = this.props
    this.setState({
      uuId:queryString.parse(location.search).uuId
    })

  }

  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        (async () => {
          values.uuId = this.state.uuId
          await this.props.submitPassword(values)
        })()
      }

    })
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value
    this.setState({confirmDirty: this.state.confirmDirty || !!value})
  }

  checkPassword = (rule, value, callback) => {
    const form = this.props.form
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Two passwords that you enter is inconsistent!')
    } else {
      callback()
    }
  }

  checkConfirm = (rule, value, callback) => {
    const form = this.props.form
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirmPassword'], {force: true})
    }
    callback()
  }

  render () {
    const {getFieldDecorator} = this.props.form
    return (

        <Form onSubmit={this.handleSubmit} className="password-reset-box">

          <FormItem
            {...formItemLayout}
            label={(<span><FormattedMessage id='密码' /></span>)}
            hasFeedback
          >
            {getFieldDecorator('newPassword', {
              rules: [{
                required: true, message: 'Please input your password!',
              }, {
                validator: this.checkConfirm,
              }],
            })(
              <Input type="password"/>
            )}
          </FormItem>
          <FormItem
            {...formItemLayout}
            label={(<span><FormattedMessage id='确认密码' /></span>)}
            hasFeedback
          >
            {getFieldDecorator('confirmPassword', {
              rules: [{
                required: true, message: 'Please confirm your password!',
              }, {
                validator: this.checkPassword,
              }],
            })(
              <Input type="password" onBlur={this.handleConfirmBlur}/>
            )}
          </FormItem>

          <FormItem>
            <Row>
              <Col span={10} offset={10}>
                <Button type="primary" size='large' className='password-reset-box__submit-btn'
                        htmlType="submit"><FormattedMessage id='确定' /></Button>
              </Col>
            </Row>
          </FormItem>

        </Form>
    )
  }
}

const PasswordResetForm = Form.create()(PasswordReset)

const mapDispatchToProps = dispatch => ({
  submitPassword: values => dispatch(submitPassword(values))
})

export default connect(() => {
  return {}
}, mapDispatchToProps)(PasswordResetForm)
