import React, { Component } from 'react'
import { Form, Input, Button, Row, Col } from 'antd'
import { connect } from 'react-redux'

import {FormattedMessage} from "react-intl";
import {convertContent} from "../../constant/lang-util";
import { submitEmail } from '../../action/user'

const FormItem = Form.Item
const formItemLayout = {
  labelCol: {
    xs: {span: 24},
    sm: {span: 8},
    lg: {span: 8},
  },
  wrapperCol: {
    xs: {span: 24},
    sm: {span: 14},
    lg: {span: 7},
  },
}

class PasswordRetrieve extends Component {
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        (async () => {
          await this.props.submitEmail(values)
        })()
      }
    })
  }
  render () {
    const {getFieldDecorator} = this.props.form
    return (

        <Form onSubmit={this.handleSubmit} className="password-reset-box">
          <FormItem
            {...formItemLayout}
            label={(<span><FormattedMessage id='邮箱' /></span>)}
            hasFeedback
          >
            {getFieldDecorator('email', {
              rules: [{
                type: 'email', message: 'The input is not valid E-mail!',
              }, {
                required: true, message: 'Please input your E-mail!',
              }],
            })(
              <Input placeholder={convertContent('请输入注册时的邮箱')}/>
            )}
          </FormItem>
          <FormItem>
            <Row>
              <Col span={10} offset={10}>
                <Button type="primary" size='large' className='password-reset-box__submit-btn'
                        htmlType="submit"><FormattedMessage id='确定' /></Button>
              </Col>
            </Row>
          </FormItem>
        </Form>
    )
  }
}

const PasswordRetrieveFrom = Form.create()(PasswordRetrieve)
const mapDispatchToProps = dispatch => ({
  submitEmail:email=>dispatch(submitEmail(email))
})

export default connect(()=>{
  return {}
}, mapDispatchToProps)(PasswordRetrieveFrom)
